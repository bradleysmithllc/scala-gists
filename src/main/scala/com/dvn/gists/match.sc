// Borrowed from http://daily-scala.blogspot.com/2010/01/matching-nulls.html
// copied here for posterity

// No surprise _ matches everything
println("Example 1")
null match { case _ => println("null matched _") }

println("Example 2")
// again null matches null
null match { case null => println("null matched null") }

println("Example 3")
// a is bound to anything including null
null match { case a => println(s"Matched value is: ${a}") }

println("Example 4")
val a:String = null
// basically same as last example
a match {case b => println(s"${b} is null")}

println("Example 5")
// Any matches any non-null object
null match {
	case a:Any => println(s"matched value is: ${a}")
	case _ => println("null is not Any")
}

println("Example 6")
val d:String = null

// In fact when matching null does not match any type
d match {
	case a:String => println(s"matched value is: ${a}")
	case _ => println("no match")
}

println("Example 7")
val data:(String,String) = ("s",null)

// matching can safely deal with nulls but don't forget the catch all
// clause or you will get a MatchError
data match {
	case (a:String, b:String) => "shouldn't match"
	case (a:String, _) => "should match"
}

println("Example 8")
// again null is all objects but will not match Any
data match {
	case (a:String, b:Any) => "shouldn't match"
	case (a:String, _) => "should match"
}