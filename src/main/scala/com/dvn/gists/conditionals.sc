/*Conditionals work as you'd expect*/
if (true) 1 else 0

/*There is a twist here.  Conditionals have a type.
* In the preceeding statement, the type is Int*/
val i = if (true) 1 else 0;

/*The return type is determined by the last statement in
 either the for or else block.  If they vary,
 the most general form is used, or Any if the compiler
 can't find one.
 */

val j = if (true) 1 else "0"

/*Nested if's likewise*/
val k = if (true) 1 else if (true) "2" else 3.0

/*If you want the return type to be void, then you
* have to add as a final statement the Unit object (Literally, '()')*/
val l: Unit = if (true) () else ()