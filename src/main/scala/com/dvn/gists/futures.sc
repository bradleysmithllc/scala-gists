import java.util.concurrent.{Semaphore, TimeUnit}

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.util.{Failure, Success, Try}

/*A future is a block of code that executes concurrently, like a Thread*/
/*It requires a stupid implicit execution context*/

implicit val iec = ExecutionContext.Implicits.global

val f = Future {
	32 + 1 - 17
}

Await.result(f, Duration(10, scala.concurrent.duration.SECONDS))

// or
import scala.concurrent.duration._
Await.result(f, 10.seconds)
Await.result(f, (10, TimeUnit.SECONDS))

println(durationToPair(10.seconds))

// the f value is not the result of the block, but the future object

println(f)
println(f.value.get.get)

val future2 = Future {
	32 + 1 - 17
}

// unwrapping future values

/*The immediate result of a future is an Option[Try[T]], where T is the result
* type of the future block*/
val futureValue: Option[Try[Int]] = f.value

// The first optional here represents the future running state.  If the value is
// Some[T], then the future has completed.  If it is None, then it is still running.
// Recall that isEmpty is a convenient way to discern them.
if (!futureValue.isEmpty)
{
	val futureValueOption = futureValue.get
}

// matching works as well
futureValue match {
	// in this case, we have a try
	case Some(v) => println(v)
	// in this one, the future has not yet completed.
	case None => println("Not ready yet")
}

// now that we have the optional value, the next layer is a Try[T], where
// the type T is again the type of the future block (Int for us)
val futureValueTry: Try[Int] = futureValue.get

// The try object will either yield a Success(v), or a Failure(ex), where
// v is my ultimate value, or ex is an Exception.  Matching looks like
// the best option here
futureValueTry match {
	case Success(i) => println(s"Finally, $i")
	case Failure(ex) => println("boo")
}

// you can also use the plain old API:
if (futureValueTry.isSuccess) println(futureValueTry.get)
if (futureValueTry.isFailure) println(futureValueTry.failed.get.getMessage)

// a try can be used for representing any block which might fail
def matchTry(tr : Try[Any]): Unit = {
	tr match {
		case Success(v) => println(s"v == $v")
		case Failure(ex) => println(s"Ex = $ex")
	}
}

/*Try's can be handled as options if you don't care about the failure scenario*/
def matchOption(tr : Try[Any]): Unit = {
	tr.toOption match {
		case Some(v) => println(s"Some(v) == $v")
		case None => println("None")
	}
}

matchTry(Try {
	1
})

matchTry(Try {
	throw new IllegalArgumentException
})

/* Using a Try to get at an optional value or a value with a default*/
val c = Try {"10".toInt} .toOption.getOrElse(17)

/* Futures can have callbacks, which yield try objects to get the value result*/
val f3 = Future {Thread.sleep(9000L); 189}
f3.onComplete(t => {matchTry(t)})
Await.result(f3, Duration.Inf)

// If a future has already completed, the callback is still invoked.
// This way, if a future is really fast, you don't have to be concerned
// if it completes before adding the callback
f.onComplete(t => {matchTry(t)})

val f9 = Future {
	Thread.sleep(10000L)
	if (Math.random() < 0.5) throw new Exception()
	42
}

val s = new Semaphore(1)
s.acquire()

f9.onComplete { t =>
	matchTry(t)
	s.release()
}

s.acquire()

Await.result(f9, Duration.Inf)

((604800000 / 1000) / 60) / 60