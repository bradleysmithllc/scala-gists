package com.dvn.gists.scala_imp

object Matrix {
	def main(argv: Array[String]) =
	{
		val mat = new Matrix(1, 3)
		mat(1, 1) = "a"
		mat(1, 2) = "b"
		mat(1, 3) = "c"

		val mat2 = new Matrix(3, 1)
		mat2(1, 1) = "x"
		mat2(2, 1) = "y"
		mat2(3, 1) = "z"

		println(mat * mat2)
		println(mat2 * mat)
	}
}

/** /
	* 2 | 3
	* 4 | 5
	*
	* 6 | 7
	* 8 | 9
	*
	* 2*6 + 3*8 | 2*7 + 3*9
	* 4*6 + 5*8 | 4*7 + 5*9
	*
	* 36  |  41
	* 64  |  73
/ **/

class Matrix(val rows: Int, val cols: Int)
{
	private val matrix: Array[Array[String]] = {
		val arr = new Array[Array[String]](rows)
		for (i <- arr.indices) arr(i) = new Array[String](cols)
		arr
	}

	def apply(row: Int, col: Int) = matrix(row - 1)(col - 1)
	def update(row: Int, col: Int, value: String): Unit = matrix(row - 1)(col - 1) = value

	override def toString = {
		val stb = new StringBuilder

		for (row <- matrix) {
			for (col <- row) {
				stb.append(col).append(" ")
			}
			stb.append("\n")
		}

		stb.toString
	}

	def *(other: Matrix): Matrix =
	{
		if (other.rows != cols) {
			throw new IllegalArgumentException
		}

		val res = new Matrix(rows, other.cols)

		for (col <- 1 to res.cols){
			for (row <- 1 to res.rows) {
				var c: String = null

				for (acol <- 1 to cols) {
					c = (if (c == null) ("") else (c + "+")) + (this(row, acol) + "*" + other(acol, col))
				}

				res(row, col) = c
			}
		}

		res
	}

	def +(other: Matrix): Matrix =
	{
		if (other.rows != rows) {
			throw new IllegalArgumentException
		}
		if (other.cols != cols) {
			throw new IllegalArgumentException
		}

		val res = new Matrix(rows, cols)

		for (row <- 1 to rows) {
			for (col <- 1 to cols){
				res(row, col) = this(row, col) + "+" + other(row, col)
			}
		}

		res
	}

	def -(other: Matrix): Matrix =
	{
		if (other.rows != rows) {
			throw new IllegalArgumentException
		}
		if (other.cols != cols) {
			throw new IllegalArgumentException
		}

		val res = new Matrix(rows, cols)

		for (row <- 1 to rows) {
			for (col <- 1 to cols){
				res(row, col) = this(row, col) + "-" + other(row, col)
			}
		}

		res
	}
}