package com.dvn.gists.implicits

/*Demonstrates that this implicit is also available in the companion class*/
object ClassPackageImplicits {
	def main(argv: Array[String]): Unit = {
		new ClassPackageImplicits().mainy
	}
}

class ClassPackageImplicits {
	def mainy: Unit = {
		println(2.fractionString)
	}
}
