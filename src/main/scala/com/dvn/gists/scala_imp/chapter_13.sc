import scala.collection.mutable.ArrayBuffer

/** Map string to indexes Exc 1 **/

val input = "Mississippi"

val seqMap = collection.mutable.Map[Char, ArrayBuffer[Int]]()

for (index <- 0 until input.length) {
	if (seqMap.contains(input(index))) seqMap(input(index)) += index else seqMap(input(index)) = ArrayBuffer(index)
}

println(seqMap)

/** immutable Map string to indexes Exc 1 **/

var imutSeqMap = Map[Char, Array[Int]]()

for (index <- 0 until input.length) {
	if (imutSeqMap.contains(input(index)))
		imutSeqMap = imutSeqMap + (input(index) -> (imutSeqMap(input(index)) :+ index))
	else
		imutSeqMap = imutSeqMap + (input(index) -> Array(index))
}

println(imutSeqMap)