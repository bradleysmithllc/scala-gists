import scala.reflect.ClassTag

implicit class IntExtensions(i: Int)
{
	def square = i * i
}

implicit class StringExtensions(str: String)
{
	def palindrome: Boolean = {str.toLowerCase == str.toLowerCase.reverse}
}

println(2.square);
println("Hi".palindrome);
println("HiH".palindrome);
println("HiiH".palindrome);

/*Context types*/

trait Enc[T]
{
	def makeInt(t: T) :Int
}

def f[T : Enc](t: T) :Int  =
{
	implicitly[Enc[T]].makeInt(t);
}

implicit val enc = new Enc[String](){
	override def makeInt(t: String) = 1
}

val u = f("Hi");
println(u)

def f[A : Numeric](a: A, b: A) = implicitly[Numeric[A]].plus(a, b)

def arr[T : ClassTag](t: T): Array[T] =
{
	Array(t)
}

val varr = arr("Hello");

for (va <- varr)
{
	println(va)
}