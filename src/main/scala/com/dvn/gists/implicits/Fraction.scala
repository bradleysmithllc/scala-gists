package com.dvn.gists.implicits

class Fraction(val numertor: Int, val denominator: Int)
{
	def fractionString(): String = {s"$numertor/$denominator"}
}
