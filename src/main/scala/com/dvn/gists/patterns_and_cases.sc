import java.io.File

import scala.collection.{JavaConversions, JavaConverters}

val s = '+' match {
	case '+' => 1
	case '-' => -1
	case _ => 0
}

println(s)

val s1 = '3' match {
	case '0' | '1' | '2' | '3' | '4' | '5' | '6' | '8' | '9' => 1
	case '-' => -1
	case _ => 0
}

println(s1)

var digit = 9

val s3 = '0' match {
	case ch if Character.isDigit(ch) => Character.digit(ch, 10)
}

println(s3)

/* Using guards in match statements.  The guard wil skip any elements which do not pass*/
val s4 = 0 match {
	case ch if ch >= 0 && ch < 10 => ch
	case ch if ch >= 10 && ch < 20 => ch * 100
}

println(s4)

val c = "Long"
val d = "Ling"

/* Referencing a constant value in a case.  Fully qualify it to avoid variable name collisions*/
val s5 = "hello" match {
	case File.pathSeparator => "blue"

	/* This is yucky.  To reference an existing variable in the match use backquotes*/
	case `c` => "blue"
	case _ => "red"
}

println(s5)

/*  Using a match / case expression to switch on the type of the variable */
for (matchVal <- Array("Hello", 1, 10.4)) {
	println(
		matchVal match {
			case s: String => "String"
			case i: Int => "Int"
			case d: Double => "Double"
		}
	)
}

/* Matches can be made on collections */
val d1 = Array("hi", "there") match {
	case Array("there") => "one"
	case Array("hi", "there") => "two"
	case Array("hi") => "three"
}
println(d1)

/* Collection matches can be generified.  The variables in the case statements are
 * placeholders for saying "This many elements".  The final form means 4 or more*/
val d2 = Array("hi", "there") match {
	/* Matches an array with 1 element*/
	case Array(x) => "one"
	/* Matches an array with 2 elements*/
	case Array(x, y) => "two"
	/* Matches an array with 3 elements*/
	case Array(x, y, z) => "three"
	/* Matches an array with 4 or more elements*/
	case Array(x, y, z, t, _*) => "four"
}
println(d2)

val d3 = Array(2, 3) match {
	case Array(0) => "one"
	case Array(1, x) => "two"
	case Array(1, 2, _*) => "three"
	case _ => "four"
}

val d4 = List(2, 3) match {
	case 1 :: Nil => "one"
	case List(1, x) => "two"
	case List(1, 2, _*) => "three"
	case _ => "four"
}

val pattern1 = "(Hello) (World)".r
val pattern2 = "(Goodbye) cruel (World)".r

for (text <- Array("Hello World", "Goodbye cruel World"))
{
	text match {
		case pattern1(verb, noun) => println(s"Pattern1: $verb, $noun")
		case pattern2(verb, noun) => println(s"Pattern2: $verb, $noun")
	}
}

for ((k, v) <- JavaConverters.propertiesAsScalaMap(System.getProperties))
{
	println(s"$k, $v")
}

/* Using guards in for comprehensions.  The guard wil skip any elements which do not pass*/
for (posI <- Array(1, 2, 3, 4, 0, -1, -2, -3) if posI > 0) println(posI)


/* Case classes can be used to check for optional values*/
val map = Map("Hello" -> "World")

for (key <- Array("Hello", "Help")) {
	map.get(key) match {
		case Some(noun) => println(noun)
		case None => println("Missing")
	}

	// or
	if (map.get(key).isEmpty) {
		println("2: Missing")
	}
	else {
		println(s"2: ${map.get(key).get}")
	}

	println(s"3: ${map.get(key).getOrElse("Missing")}")
}