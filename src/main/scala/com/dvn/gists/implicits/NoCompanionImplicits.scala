package com.dvn.gists.implicits

/*This class does not have a companion object with the implicit val defined, so it has to be supplied explicitly*/
object NoCompanionImplicits {
	def main(argv: Array[String]): Unit = {
		new NoCompanionImplicits().call(5)

		new CompanionImplicits.ExtInt(5).toCompanionImplicits.print
	}
}

class NoCompanionImplicits {
	def call(v: Int): Unit = {
		Functional.print(v)(CompanionImplicits.convert)
	}
}