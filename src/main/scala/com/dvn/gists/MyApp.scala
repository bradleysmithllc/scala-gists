package com.dvn.gists

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}
import scala.concurrent.ExecutionContext.Implicits.global

object MyApp extends App
{
	{
		println("Hello World")
	}

	val v = Future {
		Thread.sleep(10000L)
		17
	}

	v.onComplete(t => println(t))
	Await.result(v, Duration.Inf)

	Thread.sleep(1000L)
}