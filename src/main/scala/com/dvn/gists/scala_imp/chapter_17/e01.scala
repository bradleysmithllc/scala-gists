package com.dvn.gists.scala_imp.chapter_17

import scala.collection.concurrent.TrieMap
import scala.concurrent.Future

object e01 {
	def main(argv: Array[String]): Unit = {
		for (n1 <- Future {Thread.sleep(1000) ; 2};
				 n2 <- Future {Thread.sleep(1000) ; 40 } )
			println(n1 + n2)
	}
}
