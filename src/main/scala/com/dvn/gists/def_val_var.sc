/* def, var and val are all ways to declare things in Scala.
   def creates a reference value that is evaluated every time
   it is referenced, and not evaluated until it is referenced.
   1 - def is always used to declare a function.  The line
   def a = 20
   actually creates a parameterless method that returns the value 20 all the time.
*/

def a = 20
def a1() = 20
def a2: Int = 20

// func defined, but not evaluated
println("Define func")
def func() {println("func")}

// evaluate func
println("Evaluate func")
func

// func defined, and evaluated
println("Define func1")
val func1 = {println("func1")}

println("Reference func1")
func1

// lazy val changes this a bit.  With a lazy val, the value
// is not evaluated until it is referenced, but not again
println("Define func2")
lazy val func2: Int => Boolean =
{
	println("func2");
	(_ % 2 == 0)
}

println("Reference func2")
func2(1)

println("Reference func2 again")
func2(2)

class DefValVarLazyVal
{
	def value(label: String) = {println(s"value $label");Math.random()};
	def rand = value("rand");
	val randVal = value("randVal");
	var randVar = value("randVar");
	lazy val randLazyVal = value("randLazyVal");
}

println("new DefValVarLazyVal")
val d = new DefValVarLazyVal;
println("")
println("Reference randLazyVal")
println(d.randLazyVal);
println("")
println("Reference randLazyVal again")
println(d.randLazyVal);
println("")
println("Reference rand")
println(d.rand);
println("")
println("Reference rand again")
println(d.rand);

/*Early initializers allow subclasses to define values before a
* superclass constructor runs.  In this example, the sight
* value in class B is initialized to 0-length because the override
* value in B is accessed before it is initialized with 5.
* */
class A
{
	val range = 10;
	val sight = new Array[Int](range)
}

class B extends A
{
	override val range = 5;
}

val b = new B();
println(b.sight.length);

/*This one fixes the problem.  The statements before the
* 'with' in the declaration define values which are set
* before any constructors are called.  YUCK!*/
class B1 extends {override val range = 5} with A
{
}

val b1 = new B1();
println(b1.sight.length);

def fun(): Unit =
{
	new Object().synchronized {println("wait")}
}