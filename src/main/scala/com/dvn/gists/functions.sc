import java.io.File

def writeLock[A](f: => A) = f

println(writeLock {
	5
})

println(writeLock {
	"Hello"
})

println(writeLock(5))
println(writeLock("Hello"))

/* Higher order functions */
val _2 = 3.14159
val f = scala.math.ceil _
println(f(3))

/*Any Java method which takes an interface with one method can
* be invoked without creating an object.  This is known as
* SAM - Single Abstract Method.*/
for (file <- new File(".").listFiles(_.exists())) println(file)

/*Currying*/
def func(x: Int)(implicit y: Int): Unit = {println(s"${x} - $y")}

implicit val _6 = 8

val i = func(6)

def threadable(block: () => Unit): Unit = {
	new Thread(){
		override def run() {
			block()
		}
	}.start()
}

threadable {() => {println("hi")}}

def threadable1(block: => Unit): Unit = {
	new Thread(){
		override def run() {
			block
		}
	}.start()
}

threadable1 {println("hi2")}