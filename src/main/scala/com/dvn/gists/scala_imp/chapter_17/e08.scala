package com.dvn.gists.scala_imp.chapter_17

import java.net.URL

object e08 {
	def main(argv: Array[String]): Unit = {
		val url = new URL("https://www.apple.com")

		val urlF = readUrl(url)

		urlF.onComplete(tr => {
			val pat = "href=\"(https:[^\"]+)\"".r.pattern

			val text = tr.get

			val m = pat.matcher(text)

			while (m.find()) {
				println(m.group(1))
			}
		})

		Thread.sleep(10000L)
	}
}