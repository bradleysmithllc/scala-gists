import java.util.concurrent.Semaphore

/*Objects are a scala-ish way to make static or singleton objects*/
object Maths {
	def max(left: Int, right: Int) = left.max(right)
}

println(Maths.max(1, 2));

/* an Object is said to be the companion of a class if it has the same fully-qualified name,
 * and is defined within the same source file */
/* Objects are also a nice place to create builders for companion classes */
class LonelyClass {
	private val myId = 0
	private[this] val my2d = 1

	// classes can access private fields from the companion object
	def fun() = myId + LonelyClass.id

	// but not instance private fields
	//def fun2() = myId + LonelyClass.id2
}

/*The apply method of an object is used as a shorthand for constructing companion classes
* This is convention, it could be anything*/
object LonelyClass {
	private val id = 1;
	private[this] val id2 = 1;

	def apply() = new LonelyClass

	def apply(name: String) = 1

	// objects can access private fields from the companion class
	def fun(other: LonelyClass) = id + other.myId

	// but not instance private fields
	//def fun2(other: LonelyClass) = id + other.my2d
}

// LonelyClass can be created from a constructor directly:
val lc1 = new LonelyClass;

// or called through the object.  The lack of a new keyword means it calls
// LonelyClass.apply.  Note that this is still convention.  apply can return anything.
val lc2 = LonelyClass()
val lc3 = LonelyClass("Will");

// lc2 is a LonelyClass instance, lc3 is 1.

/*objects are where you can define classes that the companion class will use as
 inner classes.
 */
object C
{
	class B {}
	def apply() = new C
	def fun() = 1
}

class C
{
	var b = new C.B();
	def fun = C.fun;
}

var c1 = C()
val c2 = C()
/*This assignment would fail if the inner class B were defined in class C
* since scala treats that class type as bound to the instance, not the C type*/
c1.b = c2.b

/*objects can have a constructor as long as it is parameterless*/
object P
{
	// to construct an object, use a designated code block.
	// Technically this could be done without the braces, but don't do it.
	{
		// .. work to be done.
	}
}

/*Classes cannot be referenced solely by name*/
class P1 {}

// This makes no sense
// val p1 = P1;

// but an object can.
// c3 here refers to the actual instance object C, not the class type C
// or the type of the C object.  Note that if the parenthesis were used, as in
// C(), then the result would be a call to the C.apply function of object C.
val c3 = C; // result is the C object itself.
val c4 = C(); // result is a new C object returned from C.apply in the C object

/*Calls into the object must always be prefixed with the object name, even
* within the scope of the companion class.*/
C.fun();

/*objects can extend other classes or traits/interfaces*/
object CR extends Runnable
{
	// bad design - instance semantics in singleton!!
	private val semaphore = new Semaphore(1);
	override def run {println("Hey there"); semaphore.release()}
	def waitFor {semaphore.acquire()}
}

// single object instance conforms to the trait
CR.run

new Thread(CR).start();
CR.waitFor

/*an object with a main function can be the starting point for an application*/
object Main
{
	def main(args: Array[String]): Unit = {}
}

/*An object that extends App can be used as the entry point for an application*/
object MainApp extends App
{
	// this code block is the main application
	{
		args(0);
	}
}

// I believe I prefer the first way, explicitly declaring the main method,
// because the only thing the App trait does is mask the function call.

// enumerations in scala suck.  They don't exist in the language and must
// be rolled manually
class MyEnum private()
{
	val ValueOne: MyEnum = new MyEnum()
	val ValueTwo: MyEnum = new MyEnum()
	val ValueThree: MyEnum = new MyEnum()
	val ValueFour: MyEnum = new MyEnum()
}