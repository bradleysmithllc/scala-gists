import scala.collection.mutable.ArrayBuffer

/*Mutable sequences*/
var seq = scala.collection.mutable.Seq[Int](1, 2, 3, 4)

// append a single element with :+, or +:.  :+ means append, +: means prepend.
// This is for mutable or unmutable
seq = 2 +: seq
println(seq)
seq = seq :+ 2
println(seq)

var sequ = Seq(1, 2, 3, 4)
sequ = 2 +: sequ
println(sequ)
sequ = sequ :+ 2
println(sequ)

// the main difference between a mutable and an immutable sequence is being able to swap an
// element in the middle
seq(0) = 5

println(seq)

// does not work for immutable
// sequ(0) = 5

// concatenate collections with ++
println(seq ++ Seq(5, 4, 3, 2, 1))

/*ArrayBuffer*/
val ab = ArrayBuffer(1, 2, 3, 4)

// for array buffer, += means append to this list
ab += 1
println(ab)

// :+ means add to the end of a new collection
println(ab :+ 1)
// +: means add to the beginning of a new collection
println(1 +: ab)

val arr = ab.toArray

/*Sets*/
val set = Set(1, 2, 3, 4)

// + adds a single element.  This only works one way (coll + element)
// since sets are not ordered.  :+ and +: don't make sense.
println(set + 2)

// ++ concatenates two sets - again, unordered
println(Set(5, 6, 7) ++ set)

val set2 = Set(9, 8, 7, 6, 1, 2)

// union is |
println("Set Union " + (set | set2))

// intersection is &
println("Set Intersects " + (set & set2))

// difference is &~
println("Set Diff " + (set &~ set2))

// full difference
println("Set Diff(2) " + ((set &~ set2) | (set2 &~ set)))

/*Maps*/
var map = Map("A" -> 2, "B" -> 3)

// add an element to the map
map += ("C" -> 3)

// can be done with unapply as well, IIF this is a mutable map
//map("D") = 3

println(map)

// combine maps with ++
var map2 = Map("E" -> 5, "F" -> 6)
println(map ++ map2)

// add to maps with the single '+' operator
println(map2 + ("AA" -> 22, "BB" -> 33))

// appending an element to the end of an array
println(Array(1) :+ 2)
println(Seq(3) :+ 4)
println("P" +: Array("Hi"))
println("P" +: Seq("Hi"))
