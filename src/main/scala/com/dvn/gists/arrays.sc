import scala.collection.mutable.ArrayBuffer

/*Create a fixed-length array of ints*/
val arr = new Array[Int](10);

/*Create a fixed-length array of ints, initialized with values
* Very subtle difference here.  Notice the missing 'new' keyword.
* That means we are using Array.apply(Int*), which creates an array
* with the specific values.  If we left the new off of the definition of arr,
* we would have had an array with one value - 10.  E.G., (10).
*/
val arr2 = Array(10, 9, 8, 7, 6, 5, 4, 3, 2, 1);

/*This yields 10 0s because the array is initialized to 0*/
for (i <- arr) println(s"arr: $i")

/*Array indexing is done using parenthesis, not brackets,
* but indexes are still 0-based (Yea!)*/
arr(0) = 10
arr(9) = 1
for (i <- arr) println(s"arr<1>: $i")

/*Create a variable-length array of ints*/
val arrBuff = new ArrayBuffer[Int]();
arrBuff += 1;
arrBuff += 2;
arrBuff += 3;

/*This is empty because nothing has been added*/
for (i <- arrBuff) println(s"arrBuff: $i")

/*Copy an array.  The yield statement means make an array and return
* all values sent to the yield expression*/
val newArrBuff = for (i <- arrBuff) yield i + 1

for (i <- newArrBuff) println(s"newArrBuff: $i")

/*Filter an array.  The if statement added to the for loop
* skips the for loop body when the if condition evaluates to false
*/
val newIfArrBuff = for (i <- newArrBuff if i >= 3) yield i
for (i <- newIfArrBuff) println(s"newIfArrBuff: $i")

/*Copy array using filter.  The '_ == 1' expression is an
* anonymous function which says 'any element that equals 1'.
* The '_' is a placeholder for an unnamed variable.  Explicitly
* naming a variable is accomplished as 'i => i == 1'.  The original
* array is unmodified and a new one is returned.
*/
val fArrBuff = arrBuff.filter(_ == 1)
for (i <- fArrBuff) println(s"fArrBuff: $i")

/*transform an array using map.  Each element is modified
  by the expression '_ - 4', meaning that all elements
  will have 4 subtracted from their value.  The original
  array is unmodified and a new one is returned.
*/
val mArrBuff = arrBuff.map(_ - 4)
for (i <- mArrBuff) println(s"mArrBuff: $i")

/*transform an array using flatMap.  flatMap differs from map
  in that unlike map, flatMap is allowed to return 0, 1 or many
  values for each input value.  The return type of the transformation
  function is an array rather than a single element type.
  flatMap can be used to filter, transform or multiply elements, all in one operation.
  The original array is unmodified and a new one is returned.
*/
val fmArrBuff = arrBuff.flatMap(i => {
	ArrayBuffer(i - 1, i, i + 1)
})
for (i <- fmArrBuff) println(s"fmArrBuff: $i")

arrBuff += 4
arrBuff += 5
arrBuff += 6

/*More complex operation showing filtering, modifying and transforming.
* The input array has elements 1, 2, 3, 4, 5, 6 and the output will filter the
* 1, double the 2, pass the three through as 3 * 3, and pass the rest untouched.*/
val fmArrBuffC = arrBuff.flatMap(i => {
	var arbuffo = ArrayBuffer[Int]();

	/*
	* filter out all 1's
	* */
	if (i != 1) {
		// repeat all 2's
		if (i == 2) {
			arbuffo += i;
			arbuffo += i;
		}
		else if (i == 3) {
			// triple the value of all 3's
			arbuffo += i * 3;
		}
		else {
			// pass through all remaining elements
			arbuffo += i;
		}
	}

	arbuffo;
})
for (i <- fmArrBuffC) println(s"fmArrBuffC: $i")












val arrSteve = Array[Int](1)

for (str <- arrSteve) println(arrSteve)















