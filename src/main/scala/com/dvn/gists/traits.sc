trait Log
{
	def log(message: String): Unit
	def info(message: String): Unit = {log(s"INFO $message")}
	def warn(message: String): Unit = {log(s"WARN $message")}
	final def error(message: String): Unit = {log(s"ERRO $message")}
}

class ConsoleLog extends Log
{
	override def log(message: String): Unit = println(message)
}

val cl = new ConsoleLog

cl.log("HI");
cl.warn("HI");
cl.error("HI");