import scala.util.matching.Regex

implicit class RegexContext(sc: StringContext) {
	def r(args: Any*): Regex = new Regex(sc.parts.mkString, sc.parts.tail.map(_ => "x"): _*)
}

val p = "Hi\\.".r

val pattern = r"Hi\."

for (pat <- pattern.findAllIn("Hi.HelloHiMoreHi.")) {
	println(pat)
}

for (pat <- p.findAllIn("Hi.HelloHiMoreHi.")) {
	println(pat)
}
