package com.dvn.gists.scala_imp.chapter_17

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}

object e04 {
	def doTogether[T, U, V](f: T => Future[U], g: T => Future[V]): (T) => Future[(U, V)] = t => {
		Future {
			val u = f(t)
			val v = g(t)

			Await.result(u, Duration.Inf)
			Await.result(v, Duration.Inf)

			(u.value.get.get, v.value.get.get)
		}
	}

	def sleepLong(t: Int): Future[Long] = Future {
		Thread.sleep(5000L);

		t.toLong * 5000
	}

	def sleepString(t: Int):Future[String] = Future {
		Thread.sleep(5000L);

		s"$$${t.toString}"
	}

	def main(argv: Array[String]): Unit = {
		val cb = doTogether(sleepLong, sleepString)

		val tp = cb(5)

		Await.result(tp, Duration.Inf)

		println(tp.value.get.get);
	}
}
