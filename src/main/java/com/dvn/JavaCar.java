package com.dvn;

public class JavaCar {
	private final String make;
	private final String model;
	private final int year;
	private String licensePlateNumber = null;

	public JavaCar(String make, String model, int year, String licensePlateNumber) {
		this.make = make;
		this.model = model;
		this.year = year;
		this.licensePlateNumber = licensePlateNumber;
	}

	public JavaCar(String make, String model, int year) {
		this(make, model, year, null);
	}

	public JavaCar(String make, String model, String licensePlateNumber) {
		this(make, model, -1, licensePlateNumber);
	}

	public JavaCar(String make, String model) {
		this(make, model, -1, null);
	}

	public String getLicensePlateNumber() {
		return licensePlateNumber;
	}

	public void setLicensePlateNumber(String licensePlateNumber) {
		this.licensePlateNumber = licensePlateNumber;
	}

	public String make() {
		return make;
	}

	public String model() {
		return model;
	}

	public int year() {
		return year;
	}
}