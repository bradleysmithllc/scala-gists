package com.dvn.gists.scala_imp.chapter_17

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}

object e02 {
	def func[T, U, V](f: T => Future[U], g: U => Future[V]): T => Future[V] = {
		t => {
			val u = f(t)
			Await.result(u, Duration.Inf)

			val v = g(u.value.get.get)
			Await.result(v, Duration.Inf)

			v
		}
	}

	def f(i: Int): Future[Long] = Future {i.toLong}
	def g(l: Long): Future[String] = Future {l.toString}

	def main(argv: Array[String]): Unit = {
		val gf = func(f, g)

		println(gf(1).value.get.get)
	}
}
