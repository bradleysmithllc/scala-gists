package com.dvn.gists.scala_imp.chapter_17

import java.util.concurrent.Executors

import scala.concurrent.{ExecutionContext, Future}

object e11 {
	val ec = Executors.newCachedThreadPool()
	implicit val e = ExecutionContext.fromExecutor(ec)

	def timer(): Future[Unit] = Future[Unit] {
		Thread.sleep(10000L)

		println(System.currentTimeMillis())
	}

	def main(argv: Array[String]): Unit = {
		for (_ <- 1 to 40) {
			timer()
		}

		Thread.sleep(80000L)
	}
}