package com.dvn.gists.scala_imp

import java.net.URL

import scala.concurrent.Future
import scala.io.Source

package object chapter_17 {
	implicit val ec = scala.concurrent.ExecutionContext.Implicits.global

	def readUrl(url: URL): Future[String] = Future {
		val bs = Source.fromURL(url)

		bs.mkString
	}
}
