/*Straight looping over an index
Equivalent to this typical C-style loop construct:
for (int i = 1; i <= 10; i++)
{
   println(i);
}
*/
for (i <- 1 to 10) println(i)

/*Straight looping over an index with an exclusive end
Equivalent to this typical C-style loop construct:
for (int i = 1; i < 10; i++)
{
   println(i);
}
*/
for (i <- 1 until 10) println(i)

/*Reverse looping over an index
Equivalent to this typical C-style loop construct:
for (int i = 10; i >= 1; i--)
{
  println(i);
}
*/
for (i <- 10 to 1 by -1) println(i)

/*Alternate reverse looping over an index*/
for (i <- 1 to 10 reverse) println(i)

/*Looping with increment
Equivalent to this typical C-style loop construct:
for (int i = 1; i < 1002; i += 100)
{
   println("i: " + i + " j: " + j);
}
*/
for (i <- 1 to 1001 by 100) println(i)

/*2 dimensional (nested) looping.
Equivalent to this typical C-style loop construct:
for (int i = 1; i < 4; i++)
{
  for (int j = 1; j < 4; j++)
  {
    println("i: " + i + " j: " + j);
  }
}
*/
for (i <- 1 to 3; j <- 1 to 3) println(s"i: $i j: $j")

/*Loops can have values.  The yield statement in a loop gives the
* loop an array type*/
val j = for (i <- 1 to 2) yield i

/*Looping with a guard can be used to skip values*/
for (i <- 1 to 21 if (i % 3 == 0)) println(i)