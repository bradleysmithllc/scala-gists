package com.dvn.gists.scala_imp

import scala.beans.BeanProperty

class Car(make: String, model: String, year: Int = -1, @BeanProperty var licensePlatNumber: String = "") {
	// but the exercise requires that I supply auxilliary constructors
	def this(make: String, model: String) {
		this(make, model, -1, "");
	}

	def this(make: String, model: String, year: Int) {
		this(make, model, year, "");
	}

	def this(make: String, model: String, licensePlateNumber: String) {
		this(make, model, -1, licensePlateNumber);
	}
}