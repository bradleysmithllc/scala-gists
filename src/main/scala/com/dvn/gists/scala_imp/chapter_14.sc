import scala.annotation.tailrec
import scala.collection.mutable.ArrayBuffer
import scala.reflect.ClassTag

def swap(pair: (Int, Int)) : (Int, Int) = {
	pair match {
		case (v, x) => (x, v)
	}
}

println(swap((5, 2)))

def swap(arr: Array[Any]) : Array[Any] = {
	arr match {
		case Array(v, x, rest @ _*) => Array(x, v, rest)
		case _ => arr
	}
}

println(swap(Array(1, "Two")).mkString(";"))
println(swap(Array(1, 7, "Twenty")).mkString(";"))


abstract class Item
case class Article(description: String, price: Double) extends Item
case class Bundle(description: String, discount: Double, items: Item*) extends Item
case class Multiple(quantity: Int, items: Item) extends Item

def price(it: Item): Double = it match {
	case Article(_, p) => p
	case Bundle(_, disc, its @ _*) => its.map(price).sum - disc
	case Multiple(q, it) => q * price(it)
}

println(price(Article("b", 10.99)))
println(price(Multiple(15, Article("b", 10.99))))

val l = List(List(3, 8), 2, List(5))

def leafSum(list: List[Any]): Int = {
	(
		for (i <- list) yield
			i match {
				case i: Int => i
				case l: List[Any] => leafSum(l)
			}
	).sum
}

println(leafSum(List[Any](1)))
println(leafSum(List[Any](List[Any](1))))
println(
	leafSum(
		List[Any](
			1, 2, 3, 4, 5, List[Any](5, 6, 7, List[Any](1))
		)
	)
)

sealed abstract class BinaryTree
case class Leaf(value: Int) extends BinaryTree
case class Node(left: BinaryTree, right: BinaryTree) extends BinaryTree

def sumTree(binaryTree: BinaryTree): Int = {
	binaryTree match {
		case Leaf(v) => v
		case Node(l, r) => sumTree(l) + sumTree(r)
	}
}

println(sumTree(Leaf(1)))
println(sumTree(Node(Leaf(1), Leaf(2))))

sealed abstract class MBinaryTree
case class MLeaf(value: Int) extends MBinaryTree
case class MNode(firstNode: MBinaryTree, restNodes: MBinaryTree*) extends MBinaryTree

def sumTree(binaryTree: MBinaryTree): Int = {
	binaryTree match {
		case MLeaf(v) => v
		case MNode(tree, r @ _*) => sumTree(tree) + r.map(sumTree).sum
	}
}

println(sumTree(MLeaf(1)))
println(sumTree(MNode(MLeaf(1), MLeaf(2))))
println(sumTree(MNode(MLeaf(1), MLeaf(2))))

sealed abstract class Operation {
	def eval(lval: Double, rval: Double): Double
	def evaluate(ltree: OBinaryTree, rtree: OBinaryTree*): Double = {
		if (rtree.isEmpty) {
			eval(0.0, sumTree(ltree))}
		else {
			rtree.foldLeft(sumTree(ltree))((i: Double, rt) => {eval(i, sumTree(rt))})
		}
	}
}

case object Add extends Operation {
	override def eval(lval: Double, rval: Double) = lval + rval
}
case object Subtract extends Operation {
	override def eval(lval: Double, rval: Double) = lval - rval
}
case object Multiply extends Operation {
	override def eval(lval: Double, rval: Double) = lval * rval
}
case object Divide extends Operation {
	override def eval(lval: Double, rval: Double) = lval / rval
}

sealed abstract class OBinaryTree
case class OLeaf(value: Int) extends OBinaryTree
case class ONode(op: Operation, firstNode: OBinaryTree, restNodes: OBinaryTree*) extends OBinaryTree

def sumTree(binaryTree: OBinaryTree): Double = {
	binaryTree match {
		case OLeaf(v) => v
		case ONode(op, tree, r @ _*) => op.evaluate(tree, r : _*)
	}
}

println(sumTree(OLeaf(1)))
println(sumTree(ONode(Add, OLeaf(1), OLeaf(2))))
println(sumTree(ONode(Add, OLeaf(1), OLeaf(2))))
println(sumTree(ONode(Subtract, OLeaf(1))))
println(sumTree(ONode(Multiply, OLeaf(1))))

println(
	sumTree(
		ONode(
			Add,
			ONode(
				Multiply,
				OLeaf(3),
				OLeaf(8)
			),
			OLeaf(2),
			ONode(
				Subtract,
				OLeaf(5)
			)
		)
	)
)

def sum(list: List[Option[Int]]): Int = {
	list.map(op => if (op.isEmpty) 0 else op.get).sum
}

def sum2(list: List[Option[Int]]): Int = {
	list.map(op => op.getOrElse(0)).sum
}

println(sum(List(Option(1), Option(2), Option(4), None, Option(1))))
println(sum2(List(Option(1), Option(2), Option(4), None, Option(1))))

def compose(l: (Double) => Option[Double], r: (Double) => Option[Double]): (Double) => Option[Double] = {
	def f(d: Double): Option[Double] = {
		val ld = l(d)

		if (ld.isEmpty) None else r(ld.get)
	}

	f
}

def f(x: Double) = if (x != 1) Some (1 / (x - 1)) else None
def g(x: Double) = if (x >= 0) Some (Math.sqrt(x)) else None

val fg = compose(f, g)

println(s"${fg(2)}")
println(s"${fg(1)}")
println(s"${fg(0)}")

