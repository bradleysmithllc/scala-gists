import scala.collection.mutable.ArrayBuffer
import scala.util.Random

def printout[B](arr: Seq[B]) {
	println(arr.mkString(sep = ", ", start = "Array(", end = ")"))
}

/*
  1 - Generate array of N random integers, n between 0 and n exclusive
 */
val n = 7;
val random = new Random();
val arr = new Array[Int](random.nextInt(n)).map(_ => random.nextInt(n));

printout(arr)

/*2 - Loop to swap adjacent array elements*/
val arrl = Array(1, 2, 3, 4, 5, 10, 9, 8, 7, 6);
for (i <- 0 to arrl.length if {
	i < (arrl.length - 1) && i % 2 == 0
}) {
	def swap(xarr: Array[Int], index: Int) = {
		val e = xarr(i + 1);
		xarr(i + 1) = xarr(i);
		xarr(i) = e
	}

	swap(arrl, i);
}

printout(arrl)

/*3, redo 2 with for/yield.  How??*/
/* 4.  Take array, produce new array with positive, then 0 or negative
*  elements in original order*/
val inArr = Array(-1, 0, -2, 7, -10, 8, 9, 0, 10);
val outArr = (for (e <- inArr if e > 0) yield e);
val newOutArr = outArr ++ (for (e <- inArr if e <= 0) yield e)

printout(newOutArr)

/*5 - Compute average of Array[Double]*/
val arrD = Array(1.0, 0.0, -1.0, 4.0, 17.0, -5, 12.0, 13)

/*First parameter to aggregate is the initial value.  The result is a function which must be called
* with two parameters.  The first function operates over a partitioned set of the data, then the second
* function operates to combine the sets.*/
println(arrD.aggregate(0.0)((accumulator, value) => accumulator + value, (partitionAccumulator, partition) => partitionAccumulator + partition) / arrD.length);
println(arrD.sum / arrD.length);

/*6 - sort array and arraybuffer */
val arrBuffRaw = Array[Int](-1, 1, 8, 4, 6, 3, -2, 2, 0, 9, 8, 8, -7);
printout(arrBuffRaw.sorted);
printout(arrBuffRaw.sorted.reverse);

/*7 - sort with duplicates removed */
val sortBuff = arrBuffRaw.sorted.distinct;
printout(sortBuff);

/*8 - Remove all but the first negative number*/
// make an array of negative indexes
val arrBuff = new ArrayBuffer[Int]();
arrBuff.insertAll(0, arrBuffRaw)
val indexArr = ArrayBuffer[Int]();
indexArr.insertAll(0, for (i <- arrBuff.indices if arrBuff(i) < 0) yield i);

if (indexArr.nonEmpty) {
	indexArr.remove(0)
}
else {
	// all done.  No negatives found
}

printout(for (i <- arrBuff.indices if !(indexArr contains i)) yield arrBuff(i));