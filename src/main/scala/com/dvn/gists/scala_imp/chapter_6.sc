import java.awt.Point

/*1 - Conversions object*/

trait Converter
{
	def convert(value: Double): Double;
}

object Conversions
{
	class UOM(factor: Double, val length: Boolean)
	{
		def convertFromFactor(value: Double) = value / factor
		def convertToFactor(value: Double) = value * factor
	}

	val Centimeter = new UOM(1.0, true);
	val Inch = new UOM(2.54, true);
	val Kilometer = new UOM(100000, true);
	val Mile = new UOM(1.60934 * 100000, true);

	val Liter = new UOM(1.0, true);
	val Gallon = new UOM(1.60934, true);

	class ConverterImpl(from: UOM, to: UOM) extends Converter
	{
		override def convert(value: Double) =
		{
			to convertFromFactor (from convertToFactor value)
		}
	}

	def inchesToCentimeters(inches: Double) = inches * 2.54
	def gallonsToLiters(gallons: Double) = gallons * 3.78541
	def milesToKilometers(miles: Double) = miles * 1.60934

	def convert(from: UOM, to: UOM) = new ConverterImpl(from, to)
}

println(s"${Conversions.inchesToCentimeters(17.0)}");
println(s"${Conversions.convert(from = Conversions.Inch, to = Conversions.Centimeter) convert 17.0}");
println(s"${Conversions.convert(from = Conversions.Centimeter, to = Conversions.Inch) convert 17.0}");

/*3 - Origin object*/
object Origin extends Point
{
}

/*4 - My own point class and object*/
object point
{
	def apply(x: Double, y: Double) = new point(x, y);
}

class point(val x: Double, val y: Double)
{
}

/*5 - Hello World app*/
object HelloWorld extends App
{
	{
		for (arg <- args.reverse) println(arg)
	}
}

HelloWorld.main(Array("Hello", "World"))

/*6 - Enumeration - yuck*/
object Suit extends Enumeration
{
	val hearts = Value("H")
	val spades = Value("S")
	val diamonds = Value("D")
	val clubs = Value("C")

	/*7 - function determines if suit is red*/
	def isRed(suit: Value): Boolean = if (suit == hearts || suit == diamonds) true else false
}

/*8 - too much work.*/