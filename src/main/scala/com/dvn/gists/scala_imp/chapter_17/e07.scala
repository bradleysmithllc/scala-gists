package com.dvn.gists.scala_imp.chapter_17

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}

object e07 {
	def cores: Int = Runtime.getRuntime.availableProcessors
	def primeTarget: Int = 1000

	def splitRange(r: Range, chunks: Int): Seq[Range] = {
		if (r.step != 1)
			throw new IllegalArgumentException("Range must have step size equal to 1")

		val nchunks = scala.math.max(chunks, 1)
		val chunkSize = scala.math.max(r.length / nchunks, 1)
		val starts = r.by(chunkSize).take(nchunks)
		val ends = starts.map(_ - 1).drop(1) :+ r.end
		starts.zip(ends).map(x => x._1 to x._2)
	}

	def primes(range: Range): Future[Seq[Int]] = Future {
		var res = List[Int]()

		for (i <- range) {
			if (BigInt(i).isProbablePrime(100)) {
				res = res :+ i
			}
		}

		res
	}

	def main(argv: Array[String]): Unit = {
		val rangeSeq = splitRange(0 to primeTarget, cores)

		val futures = for (range <- rangeSeq) yield primes(range)

		val futureSeq = Future.sequence(futures)

		Await.result(futureSeq, Duration.Inf)

		val vector = futureSeq.value.get.get

		var seq = Seq[Int]()

		vector.foreach(sq => {
			seq = seq ++ sq
		})

		println(seq)
	}
}
