import java.io.File
import java.util.{Calendar, Scanner}

import scala.collection.JavaConverters._
import scala.collection.mutable

def message(label: String) {
	println("=================================")
	println(label)
	println("=================================")
}

def printout[B, C](label: String, arr: scala.collection.Map[B, C]) {
	message(label);
	for ((key, value) <- arr)
		println(s"$key: $value")
}

/*1 - Map of prices, then copy map to discount 10%*/ {
	val map = Map("iPod" -> 100.0, "iPad" -> 400.0, "iPhone" -> 500.0)
	val mapDiscounted = for ((name, cost) <- map) yield (name, cost * 0.90)

	printout("Exc1", mapDiscounted)
}

/*2 - read a file and count the number of words*/ {
	val in = new Scanner(new File("/Users/bsmith/git/scalagists/src/main/scala/com/dvn/gists/scala_imp/chapter_3.sc"))
	val mappedWords = mutable.Map[String, Int]();

	while (in.hasNext()) {
		val key = in.next();
		val current = mappedWords.getOrElse(key, 0)
		mappedWords(key) = current + 1
	}

	printout("Exc2", mappedWords);
}

/*3 - immutable map.   ??*/

/*4 - read a file and count the number of words*/ {
	val in = new Scanner(new File("/Users/bsmith/git/scalagists/src/main/scala/com/dvn/gists/scala_imp/chapter_3.sc"))
	val treeWords = mutable.SortedMap[String, Int]();

	while (in.hasNext()) {
		val key = in.next();
		val current = treeWords.getOrElse(key, 0)
		treeWords(key) = current + 1;
	}

	printout("Exc4", treeWords);
}

/*5 - read a file and count the number of words in a java TreeMap*/ {
	val in = new Scanner(new File("/Users/bsmith/git/scalagists/src/main/scala/com/dvn/gists/scala_imp/chapter_3.sc"))
	val treeWords = new java.util.TreeMap[String, Int]().asScala;

	while (in.hasNext()) {
		val key = in.next();
		val current = treeWords.getOrElse(key, 0)
		treeWords(key) = current + 1;
	}

	printout("Exc5", treeWords);
}

/*6 - Linked hash map of days*/ {
	val linkedMap = mutable.LinkedHashMap(
		"Sunday" -> Calendar.SUNDAY,
		"Monday" -> Calendar.MONDAY,
		"Tuesday" -> Calendar.TUESDAY,
		"Wednesday" -> Calendar.WEDNESDAY,
		"Thursday" -> Calendar.THURSDAY,
		"Friday" -> Calendar.FRIDAY,
		"Saturday" -> Calendar.SATURDAY,
	)

	printout("Exc6", linkedMap);
}

/*7 - system properties table */ {
	var longestKey = 0;

	for (key <- System.getProperties.keySet().asScala) {
		val valueOf = String.valueOf(key);
		if (valueOf.length > longestKey) {
			longestKey = valueOf.length;
		}
	}

	message("Exc 7");
	for ((key, value) <- System.getProperties.asScala) {
		val skey = String.valueOf(key);
		val svalue = String.valueOf(value);

		val paddedKey = skey.padTo(longestKey, ' ')
		println(f"${paddedKey} : $svalue");
	}
}

/*8 - minmax array tuple*/ {
	val arr = Array(1, 7, 4, 9, 0, 8, 3, 1)

	def minmax(arr: Array[Int]): (Int, Int) = {
		var min = Integer.MAX_VALUE;
		var max = Integer.MIN_VALUE;

		for (i <- arr) {
			min = min.min(i); max = max.max(i)
		}

		(min, max)
	}

	val (min, max) = minmax(arr);

	message("Exc8");
	println(s"min: $min max: ${max}");
}

/*9 - lteqgt*/ {
	val arr = Array(1, 7, 4, 9, 0, 8, 3, 1, -1)

	def lteqgt(arr: Array[Int], value: Int): (Int, Int, Int) = {
		var lt = 0
		var eq = 0
		var gt = 0

		for (i <- arr) {
			if (i < value) lt += 1 else if (i == value) eq += 1 else gt += 1
		}

		(lt, eq, gt)
	}

	val (lt, eq, gt) = lteqgt(arr, 0);

	message("Exc9");
	println(s"lt: $lt eq: ${eq} gt: $gt");
}

message("Exc10");
println("Hello".zip("World"))