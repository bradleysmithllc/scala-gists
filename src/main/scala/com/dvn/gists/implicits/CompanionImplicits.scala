package com.dvn.gists.implicits

object CompanionImplicits {
	/*This implicit function can be used whenever an implicit is required when the caller
	* is in the companion object.  In this case, the Functional object requires an implicit
	* parameter to print and it retrieves it from here because the caller is in the companion class*/
	implicit val convert: Converter = new Converter

	implicit class ExtInt(val v: Int) extends AnyVal {
		def toCompanionImplicits: CompanionImplicits = new CompanionImplicits(v)
	}

	def main(argv: Array[String]): Unit = {
		new CompanionImplicits(1).print

		Functional.print(5)

		5.toCompanionImplicits.print
	}
}

class CompanionImplicits(v: Int) {
	def print: Unit = println(v)
}
