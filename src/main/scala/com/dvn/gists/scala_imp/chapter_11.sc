import scala.collection.mutable.ArrayBuffer

/**
	* 1 - 3 + 4 -> 5, 3 -> 4 + 5
	*  - Leftmost operation first.  First is valid, second is not
	**/
println(3 + 4 -> 5)

//println(3 -> 4 + 5)

/** 3 - Fraction class
	*/
class Fraction(n: Int, d: Int) {
	private val numerator = if (d == 0) 1 else n * sign(d) / gcd(n, d)
	private val denominator = if (d == 0) 0 else d * sign(d) / gcd(n, d)

	override def toString = s"$numerator/$denominator"

	def sign(a: Int) = if (a > 0) 1 else if (a < 0) -1 else 0

	def gcd(a: Int, b: Int): Int = if (b == 0) a.abs else gcd(b, a % b)

	// return the inverse of this fraction
	def flip: Fraction = new Fraction(denominator, numerator)

	// multiply is straightforward
	def *(f2: Fraction): Fraction = new Fraction(f2.numerator * numerator, f2.denominator * denominator)

	// flip the other fraction and multiply
	def /(f2: Fraction): Fraction = this * f2.flip

	// cross-multiply the denominators, then add numerators together
	def +(f2: Fraction): Fraction = new Fraction(
		(numerator * f2.denominator) + (f2.numerator * denominator),
		denominator * f2.denominator
	)

	// cross-multiply the denominators, then subtract numerators
	def -(f2: Fraction): Fraction = new Fraction(
		(numerator * f2.denominator) - (f2.numerator * denominator),
		denominator * f2.denominator
	)
}

println(new Fraction(15, -6));
println(new Fraction(16, 4));
println(new Fraction(16, 4) * new Fraction(12, 20));
println(new Fraction(1, 2) / new Fraction(3, 4));
println(new Fraction(1, 2) + new Fraction(1, 4));
println(new Fraction(11, 13) + new Fraction(23, 26));
println(new Fraction(1, 2) - new Fraction(1, 4));
println(new Fraction(11, 13) - new Fraction(23, 26));

/** 4 - Money class
	*/
object Money {
	def apply(cents: Int): Money = new Money((cents / 100).toInt, cents % 100)

	def apply(dollars: Int, cents: Int): Money = new Money(dollars, cents)
}

class Money(val dollars: Int, val cents: Int) {
	def +(money: Money): Money = {
		Money(pennies + money.pennies)
	}

	def -(money: Money): Money = {
		Money(pennies - money.pennies)
	}

	def pennies: Int = cents + dollars * 100;

	def >(money: Money): Boolean = pennies > money.pennies

	def <(money: Money): Boolean = pennies < money.pennies

	def ==(money: Money): Boolean = pennies == money.pennies

	override def toString: String = s"$$$dollars.$cents"
}

println(Money(100, 0) + Money(0, 15))
println(Money(10, 25) + Money(3, 55))
println(Money(10, 25) > Money(3, 55))
println(Money(10, 25) > Money(13, 55))
println(Money(10, 25) < Money(3, 55))
println(Money(10, 25) < Money(13, 55))
println(Money(10, 25) == Money(10, 25))

/** HTML Table builder
	*/
object Table {
	def apply(): Table = new Table()
}

class Table {
	var stringBuilder = new StringBuilder
	var inHeader = true
	var inRow = false

	stringBuilder.append("<table>");

	def |(name: String): Table = {
		if (!inRow) {
			stringBuilder.append(if (inHeader) "<thead>" else "<tr>")
			inRow = true
		}

		stringBuilder.append(if (inHeader) "<th>" else "<td>")
		stringBuilder.append(name)
		stringBuilder.append(if (inHeader) "</th>" else "</td>")
		this
	}

	def ||(name: String): Table = {
		if (inHeader) {
			stringBuilder.append("</thead>")
			inHeader = false
		}
		else {
			stringBuilder.append("</tr>")
		}
		inRow = false
		this | name
	}

	override def toString: String = {
		if (inRow) {
			stringBuilder.append("</tr>")
			inRow = false
		}

		stringBuilder.append("</table>")

		stringBuilder.toString()
	}
}

println(Table() | "Java" | "Scala" || "Gosling" | "Odersky" || "JVM" | "JVM, .NET")

/** 6 - Ascii Art class
	*/
object AsciiArt {
	def apply(lines: Array[String]) = {
		if (lines.length == 0) {
			throw new IllegalArgumentException
		}

		// determine the longest line
		var longest = 0

		for (str: String <- lines) {
			longest = longest max str.length
		}

		var arr = for (line <- lines) yield line.padTo(longest, " ")

		new Array(lines.length);

		new AsciiArt(lines, longest)
	}

	def mergeArt(aArt: AsciiArt, bArt: AsciiArt): Array[String] = {
		var art = ArrayBuffer[String]();

		for (index <- 0 until (aArt.art.length.max(bArt.art.length))) {
			var leftArt: String = if (aArt.art.length > index) aArt.art(index) else pad(aArt.longest)
			var rightArt: String = if (bArt.art.length > index) bArt.art(index) else pad(bArt.longest)

			art += leftArt + rightArt
		}

		art.toArray
	}

	def pad(i: Int): String = {
		"                                         ".substring(0, i)
	}
}

class AsciiArt private(val art: Array[String], val _longest: Int) {
	def +(thatArt: AsciiArt): AsciiArt = {
		AsciiArt(AsciiArt.mergeArt(this, thatArt));
	}

	def longest = _longest

	override def toString: String = {
		var stb = new StringBuilder

		for (str: String <- art) stb.append(str).append("\n")

		stb.toString
	}
}

val a = AsciiArt(Array("Hi", "Ih", "Mo")) + AsciiArt(Array(">>>", "<<<", "+++"))
println(a)

val b = AsciiArt(Array("HIIF", "IhhF", "MooB")) + AsciiArt(Array(">>>", "<<<"))
println(b)

val c = AsciiArt(Array("HIIF", "IhhF")) + AsciiArt(Array(">>>", "<<<", "_+_"))
println(c)

val d = AsciiArt(
	Array(
		" /\\_/\\ ",
		"( ' ' )",
		"(  -  )",
		" | | | ",
		"(__|__)"
	)) + AsciiArt(
	Array(
		"   -----  ",
		" / Hello \\",
		"<  Scala |",
		" \\ Coder /",
		"   -----  "
	))
println(d)

class BitField
{
	private[this] var bitField = 0L
	def apply(bit: Int): Boolean =
	{
		(bitField & (1L << (bit - 1))) != 0
	}

	def update(bit: Int, state: Boolean): Unit =
	{
		if (bit <= 0 || bit > 64) {
			throw new IllegalArgumentException
		}

		if (state) {
			bitField |= (1L << (bit - 1))
		}
		else {
			bitField &= (0L ^ (1L << (bit - 1)))
		}
	}

	override def toString: String = {bitField.toBinaryString}
}

println("9")
var bitField = new BitField()
println(bitField)
bitField(2) = true
println(bitField(2))
println("A")

bitField(64) = true
bitField(32) = true
bitField(16) = true
bitField(8) = true
bitField(4) = true
bitField(2) = true
println(bitField)

for (i <- 64 to 1 by -1) print(if (bitField(i)) 1 else 0)
println()
