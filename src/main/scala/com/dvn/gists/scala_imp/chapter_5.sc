import com.dvn.gists.scala_imp.Student

import scala.beans.BeanProperty

/*1 - Better Counter class*/
class Counter {
	private var value = 0;

	def increment() {
		value = if (value == Int.MaxValue) 0 else value + 1;
	};

	def current = value;
}

/* 2 - BankAccount class */
class BankAccount(var _balance: Double = 0.0) {
	def deposit(amt: Double) {
		_balance += amt
	};

	def withdraw(amt: Double) = {
		_balance -= amt;
		_balance
	};

	def balance = _balance;
}

var ba = new BankAccount(10.0);
ba.deposit(2.0)
ba.withdraw(1.0)
println(ba.balance);

/*3 - Time class*/
class Time(val hour: Int, val minute: Int) {
	{
		if (hour < 0 || hour > 23) {
			throw new IllegalArgumentException("Hour must be between 0 and 23");
		}

		if (minute < 0 || minute > 59) {
			throw new IllegalArgumentException("Minute must be between 0 and 59");
		}
	}

	val julianTime = 10000 + ((hour * 100) + minute);

	def before(other: Time): Boolean = {
		if (julianTime < other.julianTime) true else false;
	}
}

val t1 = new Time(0, 0)
val t2 = new Time(1, 0)

println(t1.before(t2));
println(t2.before(t1));


/*4 - Time class minutes since midnight*/
class MsTime(val hour: Int, val minute: Int) {
	{
		if (hour < 0 || hour > 23) {
			throw new IllegalArgumentException("Hour must be between 0 and 23");
		}

		if (minute < 0 || minute > 59) {
			throw new IllegalArgumentException("Minute must be between 0 and 59");
		}
	}

	val julianTime = hour * 60 + minute;

	def before(other: MsTime): Boolean = {
		if (julianTime < other.julianTime) true else false;
	}
}

val mst1 = new MsTime(0, 0)
val mst2 = new MsTime(1, 0)

println(mst1.before(mst2));
println(mst2.before(mst1));

/*5 - Student class*/
var st = new Student("Hi", 1);
println(st.getName);
println(st.getId);

/*6 - Person class with negative age corrected to 0*/
class Person(_age: Int) {
	val age = _age.max(0);
}

var p = new Person(-1);
println(p.age);
p = new Person(1);
println(p.age);

/*7 - Person class with name parsing */
class PersonII(fullName: String) {
	val firstName = fullName.split(" ")(0)
	val lastName = fullName.split(" ")(1)
}

val p2 = new PersonII("Bradley Smith");
println(s"${p2.lastName}, ${p2.firstName}");

/*8 - Car class*/
// This is the best solution - one constructor with default
// values for the optional parameters.
class Car(make: String, model: String, year: Int = -1, @BeanProperty var licensePlatNumber: String = "") {
	// but the exercise requires that I supply auxilliary constructors
	def this(make: String, model: String) {
		this(make, model, -1, "");
	}

	def this(make: String, model: String, year: Int) {
		this(make, model, year, "");
	}

	def this(make: String, model: String, licensePlateNumber: String) {
		this(make, model, -1, licensePlateNumber);
	}
}

val car = new Car("Toyota", "Camry", 2007, "BPL-001");
car.setLicensePlatNumber("QZU-014")

/* 9 - Java Car class defined elsewhere*/
/* 10 - two Employee classes*/
class EmployeePrime(val name: String, var salary: Double) {
	def this() {
		this("John Q. Public", 0.0)
	}
}

class EmployeeTwo {
	private var _name: String = "John Q. Public"
	var salary: Double = 0.0

	def this(name: String, salary: Double) {
		this()

		_name = name;
		this.salary = salary;
	}

	def name = _name;
}