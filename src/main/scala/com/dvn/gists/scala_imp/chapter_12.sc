/* Ex 1*/
def func(eval: Int => Int, low: Int, high: Int) = {
	for (i <- low to high) yield (i, eval(i))
}

println(func(x => {x * x}, -5, 5))

/* Ex 2 */
Array(1, 2, 3, 14, 5, 6, 7).reduceLeft((x, y) => if (x > y) x else y)

println(Array(1, 2, 3, 14, 5, 6, 7).reduceLeft((x, y) => if (x > y) x else y))

/* Ex 3 */
def fact(value: Int): Long = {
	val seq = value to 1 by -1
	seq.reduceLeft((x, y) => x * y)
}

println(fact(1))
println(fact(2))
println(fact(3))
println(fact(4))

/* Ex 4 */
def fact2(value: Int): Long = {
	val seq = value to 1 by -1
	seq.foldLeft(1)((x, y) => x * y)
}

println(fact2(1))
println(fact2(2))
println(fact2(3))
println(fact2(4))
println(fact2(0))

/* Ex 5 */
def largest(fun: Int => Int, values: Seq[Int]): Int = {
	values.map(fun(_)).reduceLeft(_ max _)
}

println(largest(x => 10 * x - x * x, 1 to 25))

/* Ex 6 */
def largestIn(fun: Int => Int, values: Seq[Int]): Int = {
	val tseq = values.map(x => (x, fun(x)));
	val res: (Int, Int) = tseq.reduceLeft((l: (Int, Int), r: (Int, Int)) => if (l._2 > r._2) l else r)

	res._1
}

println(largestIn(x => 10 * x - x * x, 1 to 25))

/* Ex 7 */
def adjustToPair(f: (Int, Int) => Int): ((Int, Int)) => Int = {
	def pairF(x: (Int, Int)) = f(x._1, x._2)
	pairF _
}

def mult(x: Int, y: Int): Int = x * y

println(mult(6,7))
println(adjustToPair(mult)(6,7))

/* Ex 8 */
val strgsa = Array("Hi", "World")
val lengthsa = Array(2, 5)

strgsa.corresponds(lengthsa) {(x, y) => x.length == y}

/* Ex 9 */
def corr[T, U](left: Array[T], right: Array[U], predicate: (T, U) => Boolean) = {
	var corr = true

	for (i <- 0 until (left.length min right.length)) corr &= predicate(left(i), right(i))

	corr
}

println(corr(strgsa, lengthsa, (x: String, y: Int) => {x.length == y}))

/* Ex 10 */
class Otherwise(shouldExecute: Boolean)
{
	def otherwise(block: => Unit): Unit = if (shouldExecute) block else ()
}

def unless(condition: => Boolean)(block: => Unit): Otherwise = {
	if (!condition) block else ()
	new Otherwise(condition)
}

unless (1 == 1) {
	println("1")
} otherwise {
	println("2")
}