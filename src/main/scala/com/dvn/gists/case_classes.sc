class Bitfield{}

case class ByteBitfield(field: Byte) extends Bitfield {}
case class ShortBitfield(field: Short) extends Bitfield {}
case class IntBitfield(field: Int) extends Bitfield {}
case class LongBitfield(field: Long) extends Bitfield {}

/*Case classes sort of combine type matching and value matching*/
for (bf <- Array(ByteBitfield(0x78), ShortBitfield(9), IntBitfield(17889), LongBitfield(9L), "Hello")) {
	bf match {
		case ByteBitfield(b) => println("ByteBitfield")
		case ShortBitfield(b) => println("ShortBitfield")
		case IntBitfield(b) => println("IntBitfield")
		case LongBitfield(b) => println("LongBitfield")
		case str: String => println("String")
	}
}
