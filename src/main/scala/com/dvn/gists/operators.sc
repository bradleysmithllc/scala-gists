// Adding an operator to an existing class
implicit class StringOp(str: String)
{
	def +-(): String = str + "+-"
}

println("Hi"+-)

class MyVal(var i: Int)
{
	def ++(): Int = {i += 1; i}
}

println(new MyVal(1)++)

implicit class FakeInt(var i: Int)
{
	def ++(): Int = i + 1
}

println(1++)