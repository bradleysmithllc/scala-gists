package com.dvn.gists.implicits

/*This class demonstrates that implicit conversions defined in the package
* object for this package are available to classes or objects in the package*/
object PackageImplicits {
	def main(argv: Array[String]): Unit = {
		println(2.fractionString)
	}
}