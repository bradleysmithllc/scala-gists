trait Item
{
	def price: Double
	def description: String
}

class SimpleItem(val price: Double, val description: String){}

class Point(x: Int, y: Int)
class LabeledPoint(x: Int, y: Int, label: String) extends Point(x, y)