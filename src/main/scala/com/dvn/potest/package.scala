package com.dvn

import java.util.{Map => JavaMap}

import scala.collection.mutable
import scala.collection.JavaConverters._

package object potest {
	def visible = 1

	def copyMap[K, V](javaMap: JavaMap[K, V]): mutable.Map[K, V] = for ((key: K, value: V) <- javaMap.asScala) yield (key, value)

	private[this] val a: Int = 1664525
	private[this] val b: Int = 1013904223
	private[this] val upperLimit: Long = BigInt(2).pow(32).toLong

	var seed: Long = 0

	def nextLong: Long =
	{
		seed = ((seed * a) + b) % upperLimit
		seed
	}

	/* double is between 0 and 1 */
	def nextDouble: Double = (BigDecimal(nextLong) / BigDecimal(upperLimit)).doubleValue()
}

object test
{
	def main(args: Array[String]): Unit =
	{
		for (_ <- 1 to 1000) println(potest.nextDouble);
	}
}