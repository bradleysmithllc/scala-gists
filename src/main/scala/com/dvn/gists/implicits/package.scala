package com.dvn.gists

package object implicits {
	/*Implicit conversion for int's to fraction objects for all classes in this package*/
	implicit def int2Fraction(in: Int): Fraction = new Fraction(in, 1)
}
