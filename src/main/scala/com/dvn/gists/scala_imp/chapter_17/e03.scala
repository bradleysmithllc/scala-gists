package com.dvn.gists.scala_imp.chapter_17

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}

object e03 {
	def func[T](f: (T => Future[T])*): T => Future[T] = {
		t => {
			var u: Future[T] = null

			for (fc <- f) {
				u = fc(if (u == null) t else u.value.get.get)
				Await.result(u, Duration.Inf)
			}

			u
		}
	}

	def f(i: Int): Future[Int] = Future {i + 2}
	def g(l: Int): Future[Int] = Future {l + 4}

	def main(argv: Array[String]): Unit = {
		val gf = func(f, g, f, g, f, g, f, g)

		println(gf(1).value.get.get)
	}
}
