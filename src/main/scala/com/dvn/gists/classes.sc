import scala.beans.BeanProperty


/** ***************************
	* Properties
	* **************************/
/**
	* Scala generates setters/getters whenever it can.
	* This does NOT mean encapsulation, since
	* the result is exactly the same as public variables.
	**/
/*property myself is accessed through an automatically
* generated getter/setter pair*/
class cl_var {
	var myself = 1;
}

var c = new cl_var();

// this uses implicit cl.myself()
println(c.myself);

// this uses implicit cl.myself_= (!!?)
c.myself = 0;
println(c.myself);

/*Since myself is a val, it is generated
* as private and no setter is made*/
class cl_val {
	val meself = 3;
}

var cl2 = new cl_val()
println(cl2.meself);

/*Manually defined property.  Make the property
* private then create your own getter and/or setter*/
class cl_c {
	private var _i = 12;

	def i = _i;
}

val cc = new cl_c();
println(cc.i);

/*private fields in a class can be normal, meaning
* other objects of the same type can access, or object private
* meaning only this instance can access.*/
class cl_priv {
	private var classVal = 9;
	private[this] var objectVal = 10;

	def fun = {
		classVal = 10;
		objectVal = 11;
	}

	def funny(that: cl_priv) {
		// valida because these are my instance fields
		classVal = 11;
		objectVal = 12;

		// valid because this field is class private
		that.classVal = 13;

		// not valid because that field is private
		// to that object
		// that.objectVal = 14
	}
}

/*Properties can be forced to use bean semantics
* in addition to scala property semantics*/
class cl_beans {
	@BeanProperty
	var b = 17;
}

val c3 = new cl_beans();

println(c3.b);
println(c3.getB());

c3.setB(9)

println(c3.b);
println(c3.getB());

c3.b = 19

println(c3.b);
println(c3.getB());

/** ***************************
	* Constructors
	* **************************/
// There is a primary constructor (name: String)
// that all constructors must ultimately chain to
// the name value in the primary constructor
// becomes a property in the class just like
// a regular one.
class cl_const(val name: String) {
	// auxillary constructor - must
	// defer to another auxillary constructor
	// or the primary one
	def this() {
		this(name = "None")
	}

	// this one calls the primary one as well
	def this(iname: Int) {
		this(name = iname.toString)
	}

	// this one defers to another aux const
	def this(dname: Double) {
		this(iname = 1)
	}

	// accessor for name
	def whoAmI = name;
}

val cca = new cl_const(name = "Deary");
val ccb = new cl_const(iname = 1);
val ccc = new cl_const(dname = 1);
val ccd = new cl_const();

/*Primary constructor params can have default values,
* making them appear as alternate constructors.
* They also follow the same rules as declared properties,
* with the exception that they are object-private by default*/
class cl_prim(private[this] val name: String = "None", private[this] val age: Int = 0) {
	def myAge = age;

	def myName = name;
}

val cec = new cl_prim();
val cec2 = new cl_prim(name = "Yeller");
val cec3 = new cl_prim(age = 17);
val cec4 = new cl_prim(age = 27, name = "Oldie");

/** Primary constructors can be private **/
class cl_prim_priv private(name: String) {
	// cannot be created without an auxillary constructor
	def this() {
		this("Me")
	}
}

val cec5 = new cl_prim_priv();

//!!!!!!!!!!!!!!!!!!!
// This section does not appear to be true.
// It is correct, just weird looking.  The
// java types are the same, but scala limits access.  Below
// enc1.i cannot be assigned to anc2.i
/*Classes nested within other classes are typed to the instance,
* not the enclosing type.*/
class Enc {

	class Inner {}

	var i = new Inner();
}

val enc1 = new Enc;
val enc2 = new Enc;

// not only are enc1.i and enc2.i different objects, but they are different
// types
println(enc1.i.getClass);
println(enc2.i.getClass);
println(enc1.i.getClass == enc2.i.getClass);

// to allow an inner class to access the outer instance
// using shorthand, define it as a block
class Enc2(name: String) {
	outer =>

	class Inner {
		def this(me: String) {
			this()
			println(outer.name);
			//or
			println(Enc2.this.name);

			println(me);
		}
	}

	val inner = new Inner(me = "You");
}


val d = new Enc2(name = "Valerie")

/*The body of the class is the default constructor -
* when the class is constructed the code in the class
* is executed from the top to the bottom.*/
class cl_cost_body(name: String) {
	// Braces are not required here - but I think
	// it makes it explicit that this code is running
	// on initialization - like a Java initializer.
	// Code just lingering within a class is messy
	{
		if (name == "Hello")
			throw new IllegalArgumentException
	}

	val myName = if (name == "Too") "Me" else "Too"
}


/*the special method 'apply' is called whenever you use the parenthesis
* operator*/
class Applicable
{
	def apply() = "Applied"
}

val app = new Applicable()()
println(s"app: $app");

/** Superclass constructors may not be called explicitly
	  by the subclass, as in java super(...).  They
	  are called on the extends defintion.*/
class SuperWhatever
{}

class SubWhatever extends SuperWhatever{}

// The SubWhatever primary constructor, which is emtpy, calls the
// SuperWhatever primary constructor, which is also empty

class SuperSomething(name: String, id: Int)
{
	def this(nameTag: String)
	{
		this(nameTag, 1);
		println("ByTag");
	}
}

// auxiliary constructors may only call primary constructors in this class, not a super
// but the extends clause my link to an auxiliary constructor
// in this case sub primary calls super auxiliary
class SubSomething(name: String, id: Int, some: String) extends SuperSomething(some)
{
	def this()
	{
		this("Name", 17, "Tag")
	}
}

// in this one sub primary calls super primary
class SubSomethingElse(name: String, id: Int, some: String) extends SuperSomething(name, id)
{
}

val sub1 = new SubSomething();
val sub2 = new SubSomethingElse("MyName", 23, "Something");

// anonymous classes are similar to java . . .
val sub3 = new SubSomething() {override def toString = "Anonymous type . . . "}

// abstract classes can be defined without any methods (But why -
// this is an interface or trait with no operations.
abstract class SubAbs
{
}

// the common case is to declare a class abstract because it doesn't make sense
// as an instance.  Animal is abstract because there is no instance of animal,
// every single animal is an instance of some type of animal
abstract class Animal(typeName: String)
{
}

// super-class constructors may use literals
class Ferret extends Animal("Ferrous")
{
}

// abstract class with abstract methods
abstract class Transaction(dollarAmount: Double)
{
	// two abstract operations, and one abstract value
	def rollback: Unit
	def commit: Unit
	val fees: Double
}

class DollarTransaction(dollars: Double) extends Transaction(dollars)
{
	def rollback {/* throw this one away */}
	def commit {/* make it permanent */}
	val fees = 10.0d // standard fee
}