
class Matrix(val rows: Int, val cols: Int)
{
	private val matrix: Array[Array[Int]] = {
		val arr = new Array[Array[Int]](rows)
		for (i <- arr.indices) arr(i) = new Array[Int](cols)
		arr
	}

	def apply(row: Int, col: Int) = matrix(row)(col)
	def update(row: Int, col: Int, value: Int): Unit = matrix(row - 1)(col - 1) = value

	override def toString = {
		val stb = new StringBuilder

		for (row <- matrix) {
			for (col <- row) {
				stb.append(col).append(" ")
			}
			stb.append("\n")
		}

		stb.toString
	}

	def +(other: Matrix): Matrix =
	{
		if (other.rows != rows) {
			throw new IllegalArgumentException
		}
		if (other.cols != cols) {
			throw new IllegalArgumentException
		}

		val res = new Matrix(rows, cols)

		for (row <- 0 until rows) {
			for (col <- 0 until cols){
				res(row, col) = this(row, col) + other(row, col)
			}
		}

		res
	}
}

val mat = new Matrix(10, 9)
val mat2 = new Matrix(10, 9)
mat(8, 9) = 7
mat(9, 8) = 7
mat(10, 7) = 7
mat2(1, 7) = 7
mat2(2, 6) = 7
mat2(3, 5) = 7

System.out.println(mat2.toString)

new Matrix(2, 2) +(new Matrix(3, 2))

println(mat)
println(mat2)
println(mat + mat2)
println(mat + mat2)
println(mat + mat2)
println(mat + mat2)