package com.dvn.gists.scala_imp.chapter_17

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}

object e05 {
	def seqFuture[T](futures: Seq[Future[T]]): Future[Seq[T]] = {
		Future.sequence(futures)
	}

	def makeFuture(value: String): Future[String] = Future { value }

	def main(argv: Array[String]): Unit = {
		val seq = seqFuture(Array(makeFuture("Hello"), makeFuture("World")))

		Await.result(seq, Duration.Inf)

		println(seq.value.get.get)
	}
}
