package com.dvn.gists.implicits

object Functional {
	def print(i: Int)(implicit converter: Converter): Unit = {
		converter.convert(i).print
	}
}

class Converter {
	def convert(v: Int): CompanionImplicits = new CompanionImplicits(v)
}