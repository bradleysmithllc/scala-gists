/*Implicit values are values that a function may require
* but the caller does not have to explicitly send them - but it may*/
class SomeUsefulContext {
	def call(value: String): Unit = println(value)
}

def callMe(value: String)(implicit con: SomeUsefulContext): Unit = {
	con.call(value)
}

/*Call me can be called with the context explicitly*/

callMe("Hello")(new SomeUsefulContext)

/*Or, the context can be declared as an explicit value and retrieved by the
* function through the call chain*/
implicit val sC = new SomeUsefulContext

callMe("World")

/*implicit values can be retrived from:
*  1 - Any implicit object already in scope
*  2 - The scope of the current method.  (Declared stack or member value
*  3 - The companion object of the class executing the method
*  4 - The package object of this class
*  */

/*Implicits can also be defined as conversion functions -
* like copy constructors in C++.  These conversion functions
* can make it appear that you are adding functionality to a class,
* as in extensions in swift, but they are primitive in nature and cannot
* change the state of the type being converted*/

/*Conversion classes should extends AnyVal so that the class itself
* is inlined and never created.  It is commented out here since the
* worksheet is apparently implemented in a class which makes the declaration illegal*/
implicit class StringExtension(val string: String) //extends AnyVal
{
	def bad_length: Int = string.length + 2
}

/*Now, whenever I see a string, there appears to be a new
* method available called bad_length.  This call is actuall
* a call to the implicit conversion function defined in StringExtension*/

println("Hello".bad_length)